[[!table header="no" class="mointable" data="""
 [[Home|FrontPage]]  |  [[TiNDC 2006|IrcChatLogs]]  |  [[TiNDC 2007|IrcChatLogs]]  |  [[Old Logs|IrcChatLogs]]  |  [[Current Logs|IrcChatLogs]]  | DE/[[EN|Nouveau_Companion_31]]/[[ES|Nouveau_Companion_31-es]]/[[FR|Nouveau_Companion_31-fr]]/RU/[[Team|Translation_Team]] 
"""]]


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 23 de noviembre


### Introducción

Querida audiencia, bienvenida de nuevo al último número del TiNDC. La anterior edición llamó la atención de algunos sitios importantes de Alemania. Aunque resultaba un poco precipitado decir que el 2D está acabado, la realidad es que prácticamente lo está, exceptuando el establecimiento del modo de video y, por tanto, mejor soporte de Randr1.2. En estos momentos puedes tener suerte con las NV4x y NV3x, pero las tarjetas más antiguas y las NV5x no funcionan demasiado bien. 

* [[http://www.golem.de/0711/55966.html|http://www.golem.de/0711/55966.html]]  (en Alemán)  
 [[http://www.pro-linux.de/news/2007/11966.html|http://www.pro-linux.de/news/2007/11966.html]] (en Alemán) 
Mientras que el trabajo en 2D se va reduciendo progresivamente (salvo los casos anteriores), la atención se va dirigiendo al 3D. Darktama y jkolb están trabajando en temas de Gallium y DRM / TTM. Marcheu todavía está peleándose con una plataforma genérica para dar soporte a tarjetas antiguas en Gallium3D. Ahuillet, pmdata y p0g están a la espera de que la remate para ir añadiendo funcionalidades 3D. 


### Situación actual

Marcheu ha estado ocupado las últimas semanas, trabajando con Benh y otros probadores en errores de PPC. Al hacer transferencias DMA de subida o bajada a pantalla, DFS ([[DownloadFromScreen|DownloadFromScreen]]) / UTS ([[UploadToScreen|UploadToScreen]]), estas se colgaban, pero, por lo demás, las transferencias DMA funcionaban, mientras no se activase AGP. IFC (ImageFromCPU) tenía un comportamiento errático, al indicar error cuando en realidad todo funcionaba correctamente. Este error fue señalado por Benh, que continuó ayudándolos a dejar funcionando los PPC de todos nuestros probadores. Además, Marcheu consiguió acceso remoto a una máquina PPC para hacer pruebas. Está esperando en estos momentos a obtener volcados de registros que producen cuelgues y bloqueos de la máquina. Por eso necesita una persona delante del teclado de la máquina remota. 

Estos volcados esperamos que permitan hacer funcionar nouveau en la máquina de chimaera, que todavía experimentaba algunos cuelgues de nouveau en PowerPC. Tras  intercambiar algunos parches de prueba, marcheu logró hacer que nouveau se colgase un poco más tarde durante la inicialización. Resultó que todo se debía a algunos problemas de orden de bits en el intérprete de la bios. Por lo que el error se remitió a malc0 , que arregló algunos fallos de su intérprete. 

Los arreglos del intérprete solucionan los problemas de orden de bits, además de algunas aclaraciones del código (conocidas como limpieza). Junto con stillunknown, hicieron un poco más de limpieza en el código randr1.2 y arreglaron otros fallos varios. 

Stillunknown sigue intentando hacer funcionar randr1.2 en la máquina de AndrewR. Le envió numerosos parches pero todavía no se ha encontrado solución a los problemas en su tarjeta NV3x. 

Aunque stillunknown detectó numerosos fallos, intercambió opiniones y sabiduría con Thunderbird acerca de los distintos registros que controlan la salida de video. El intercambio les resultó fructífero pero los parches resultantes de stillunknown no lograron su efecto en la máquina de AndrewR. ¿Lograrán finalmente nuestros expertos en modos de video su porción de fama en nouveau?. Por favor, no dejéis de seguir el próximo número del TiNDC, donde se descubrirá todo... (o, por lo menos, algo más :) ). 

Thunderbird decidió lanzarse a descubrir cómo funciona TV-OUT. Añadió una petición de ayuda para hacer pruebas en nuestra página de "Se buscan probadores". 

jb17some se atrevió a meterse con XvMC. Aunque no hemos vuelto a recibir señales de vida en el IRC, su página ha incorporado alguna actualización. De acuerdo a ésta, escribió un pequeño programa que actúa igual que si se usase XvMC. Es decir, la entrada se sitúa en un buffer, se escriben algunos registros, y el resultado final es una imagen que se podría mostrar. Si se compara el estado de los registros y la salida, su programa genera los mismos resultados que el driver nv, en las mismas circunstancias. 

Ha de señalarse que su programa de prueba no genera ninguna imagen. 

Umm, regresemos a los asuntos de marcheu y veamos lo que está haciendo: parece seguir ocupado escribiendo la plataforma Gallium3D para tarjetas antiguas. Tendremos que echar otro vistazo más adelante... 

Tras resultar vencido por el equipo NV3x en la carrera por lograr hacer funcionar antes A8+A8 (y, por tanto, aceleración EXA completa), ahuillet quería evitar que le llamasen "perdedor" por no ser capaz de lograr esa "sencilla" tarea ;). 

En serio ahora, la tarea distó mucho de ser sencilla. Para acelerar la [[PictOpt|PictOpt]], era necesario cumplir numerosas condiciones (por ejemplo, límites de anchura, o de la posición de destino). Tras algunas charlas con [[IronPeter|IronPeter]] y Marcheu sobre lo que se podría hacer a través de los combinadores de registros, ahuillet implementó un apaño bastante feo que logró hacer funcionar tras unas cuantas rondas de pruebas y parches. 

[[!img http://people.freedesktop.org/~ahuillet/a8_plus_a8_mmh.png] 

_A8+A8 malfuncionando_ 

Un día después afirmó: 1) que todo funcionaba y 2) que había refactorizado el apaño en algo legible. p0g y AndrewR confirmaron que EXA funcionaba. 

Unos días más tarde, llegaron informes de que Xv no funcionaba en PPC y que había algunos problemas relacionados con la corrupción de fuentes. Ya que marcheu estaba a la caza de un montón de fallos de PPC (ver más arriba), se decidió que antes se eliminarían los fallos más básicos y, si los problemas con Xv todavía persistían, entonces ahuillet los solucionaría. Sin embargo, los problemas con las fuentes están muy arriba en la lista de tareas pendientes de ahuillet. 

[[!img http://people.freedesktop.org/~ahuillet/victory_final.png] 

_En funcionamiento, por lo menos en X86_ 

Darktama hizo algunas tareas de limpieza en el DRM y encontró la oportunidad para introducir un error de 64bit que impedía el funcionamiento de nouveau en los x86. Marcheu localizó el problema y lo solucionó un par de horas más tarde. 

jkolb todavía trabaja en el TTM. 

Así que, tal vez, podemos conseguir que marcheu nos indique una fecha definitiva para la versión del driver 2D. Vamos a probar a preguntarle... bien... Parece que está trabajando en refactorizar el código Xv en una parte de código genérica y otra particular de cada tarjeta. Eso nos va a permitir añadir código de Xv hecho con shaders con más facilidad en las tarjetas NV4x y NV5x. Y además es necesario solucionar los errores de PPC que mencionamos antes... Parece que nos va a llevar un poco más de tiempo hasta que podamos anunciar que el soporte 2D está completado. 


### Ayuda necesaria

En nombre del equipo Nouveau me gustaría mostrar nuestro agradecimiento a todas las personas que se han ofrecido en el canal IRC a hacer pruebas. Se solucionaron muchos problemas y otros están en camino de serlo. Al precisar algunos de estos problemas numerosos ciclos de prueba, algunas personas mostraron una dedicación elogiable. 

stillunknown busca personas que tengan una tarjeta 7xx0 que puedan hacer pruebas con su ayuda. Además, está buscando gente que tenga doble salida (dualhead), en cualquier combinación de VGA y DVI y que quieran probar el establecimiento de modo con randr1.2. 

Marcheu necesita voluntarios para hacer pruebas con NV30 (no valen modelos posteriores) ya que las cosas son un poco distintas en esa tarjeta. 

Y si tienes interés en ayudar a Thunderbird en su lucha con TV-Out, por favor, echa un vistazo aquí: [[http://nouveau.freedesktop.org/wiki/TestersWanted|http://nouveau.freedesktop.org/wiki/TestersWanted]] Esta página contendrá información actualizada sobre las solicitudes de pruebas y ayuda de los miembros del equipo, por lo que, por favor, echad un vistazo. ¡Necesitamos gente para hacer pruebas continuamente! 
