<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="/wiki/translate.js"></script>

### Renouveau dumps

It is unlikely that REnouveau dumps will turn up new information at the moment, but Fermi (not Enrico) will probably change that soon. REnouveau will first have to be adjusted, so don't rush or the dumps will be useless. 

Check the following section to see if you have something specific. You can then follow the guidelines to provide a proper REnouveau dump. 


#### Call for specific dumps

* NVAx dumps need to be redone with an up-to-date renouveau containing [[that fix|http://nouveau.cvs.sourceforge.net/viewvc/nouveau/renouveau/family.c?revision=1.8&view=markup]] 
* any nvidia card plugged into a non-x86 machine (to check for endianness issues) 

#### How to run Renouveau

For the install procedure see the page [[REnouveau|REnouveau]]. 

When you run REnouveau, make sure screensaver does not activate, do not wave your mouse over the REnouveau decorationless window and most importantly make sure the whole window is visible all the time. Do not try to move or resize the window. Using Compiz or Beryl, having Aiglx, or similar things (3D accelerated) are likely harmful while running Renouveau. 


#### Don't submit dumps when renouveau crashes

The dumps will be incomplete (not all the tests passed). It's better to contact us on IRC so we can fix it for your hardware. Incomplete dumps mean we won't have the full information to support your card. 

For now, there is sometimes problem (renouveau crashes) with 9*** drivers, PCI-E cards and renouveau (according to marcheu: "that's a PCI-E issue - with the 9xxx driver series, it maps stuff in and out of the process dynamically"). 


#### Renouveau displays some OpenGL errors

The tests are very simple ones to test some OpenGL rendering functions, it may not be totally safe. If you know OpenGL coding, you could help fixing the tests, so they won't display any errors. Most likely it's testing unvailable stuff, or the OpenGL context is not correctly setup to test a specific feature. 


[[!format txt """
"Latest OpenGL Error: 1282" or similar
"""]]
If you are just making a dump, it is safe to ignore these. 


#### Archive filename

Please use PCI device id when sending dumps archive to gmail address. So we can easily spot multiple dumps for the same card. The PCI device id is outputted by 'lspci -n': 


[[!format txt """
0000:01:00.0 0300: 10de:0151 (rev a4)
"""]]
* 0300 is the pci class for vga display devices. 
* 0x10de is the vendor id for Nvidia. 
* 0x0151 is the device id for Geforce 2 Ti. 
So name your dump archive 10de0151.tar.bz2 (the 0151 part is likely different for you). 


#### Archive content

Put only card_stdout.txt, card_objects.txt, card_mappings.txt and all other card_<pciid>_test_*.txt files into the archive, using the name above. 


[[!format txt """
$ tar cvjf 10de0151.tar.bz2 card*.txt
"""]]
Please be sure you name it something like ".tar.bz2" (no quotes), NOT "Nvidia_blarg.tgz". My script is not made to handle "tgz" extensions, and anyone who has submitted a dump this way should rename the file to be ".tar.gz" and send it in again (sorry for any inconvenience). 


#### Sending archive

Just email it to _renouveau <dot> dumps <åt> gmail.com_. 


#### Status of send dumps

After a couple of days, your dumps should turn up in the current dump list at: [[http://nouveau.freedesktop.org/tests/|http://nouveau.freedesktop.org/tests/]] You can check that it was you who send a dump by following the link of your card, and then open the submitter.txt link. 
