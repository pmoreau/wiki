
[[English|Nouveau_Companion_16]]/Español/[[Français|Nouveau_Companion_16-fr]] 


## Informe irregular sobre el desarrollo de Nouveau


## Edición del 17 de Marzo


### Introducción

Bueno, ¿qué podemos decir? Obviamente cometimos dos errores que nos tomaron por sorpresa: 

1. Pedimos volcados en el último TiNDC (Informe irregular sobre el desarrollo de Nouveau). 
1. Incorporamos una nueva noticia sobre una lista generada por JussiP, que muestra las tarjetas de las que ya existe un volcado y de las que no. 
Eso puso las cosas en marcha. Digg.com tomó el testigo y linuxfr.org también, así como un artículo en un fóro de ubuntu. Y luego el canal (IRC) se inundó de preguntas sobre cómo realizar los volcados. PQ, Pmdata y marcheu trataron duramente de ayudar, pero era demasiado y marcheu se rindió primero, desapareciendo (el resto de ese día, al menos), mientras que pq y pmdata tuvieron que abandonar sus intenciones de llevar a cabo cualquier desarrollo. 

No sabemos qué otra cosa decir más que "Gracias!". No nos deja de sorprender continuamente la voluntad de la comunidad de ayudarnos. El 12.03.2007 teníamos ya 426 nuevos volcados para diversas tarjetas (incluso tarjetas 8800). 

Si quieres participar en el SoC de Google por favor lee [[SoC|SoC]] y la sección de ayuda adicional de más abajo. Por favor, ponte en contacto con nosotros antes del 24.03.2007 si estás interesado/a. 


### Estado actual

Nuestro script de subida de gmail a sf.net se estropeó en algún momento de las últimas dos semanas. Resultó obvio tras la llegada del antes mencionado aluvión de volcados. Kmeyer, que escribió el script, mencionó algo como "escaso ancho de banda" antes de tirar la toalla. Marcheu retomó el asunto y lo hizo funcionar en su sistema, así que, tras 4 días desde que nos dimos cuenta del problema, las actualizaciones del directorio de volcados volvió a funcionar de nuevo. Sin embargo, el script de volcados no funcionaba correctanmente y sólo encontraba un pequeño conjunto de los volcados que habían sido enviados. Por lo que kmeyer volvió de nuevo a depurarlo y encontró algunos problemas con ciertos clientes de correo que emplean cabeceras extrañas. Tras algún trabajo, el script acabó funcionando de nuevo (y por ello aparecen ya los nuevos 426 volcados). 

Fue algo importante ya que JussiP creó una página en [[http://users.tkk.fi/~jpakkane/ren/|http://users.tkk.fi/~jpakkane/ren/]] donde se muestra el estado del directorio de volcados. No se produjeron cambios en él durante un tiempo y una nota en la página de entrada que dijese "Demasiados volcados" resultaría sospechoso :). Sin embargo, desde el 15.03.2007 la página debería reflejar correctamente el estado real de los volcados. 

Durante el aluvión de volcados renouveau mostró algunos problemas: En sistemas PCI-e la herramienta tiene muchas posibilidades de romperse. Una indagación en el error reveló que los drivers más recientes de NVidia reasignan algunos registros de E/S al vuelo, mientras que renouveau sigue intentando acceder a las localizaciones antiguas que ya no son válidas. Un intento de solución del fallo por parte de marcheu no logró solucionarlo, y ,en estos momentos, únicamente podemos pedir que se utilice un driver previo a la versión 8776 para evitar estos problemas. 

Algunas personas informaron sobre errores de OpenGL ("Latest OpenGL error: xxxx") que, en algunos sistemas, incluso derivaban en errores del driver (Xid: ....) binario de NVidia. Pmdata, KoalaBR y pq se ofrecieron para arreglar las pruebas, de forma que desapareciesen los errores de OpenGL. jb17bsome investigó las razones para que se produjesen los errores Xid y parece que, en algunos casos, tratar de localizar un objeto en RAMIN a través de INSTANCE_MEM() resultaría en ese error. En el momento de escribir estas líneas, el asunto todavía no se ha resuelto, pero sigue bajo la indagación de jb17bsome y marcheu. 

Ahora tenemos nuestras entradas en el Bugzilla de freedesktop.org. Si tienes problemas con nuestro controlador 2D y/o 3D, por favor, informa de él allí. Tampoco está de más dar cuenta de él en el canal (IRC), pero no esperes que nadie ayude o haga algo inmediatamente. Sin embargo, ¡los desarrolladores lo verán!. Bugzilla está disponible [[aquí|https://bugs.freedesktop.org/]][[[https://bugs.freedesktop.org/|https://bugs.freedesktop.org/]] ]. 

Airlied ha actualizado la rama randr1.2 de modo que incluya todas las mejoras de la rama principal. Sus pruebas del nuevo código mostraron algunas regresiones que pueden ser debidas tanto a la reconciliación de cambios o a su propio desarrollo. La corrección de errores y la realización de más pruebas están sin embargo limitadas por la falta de tiempo de airlied, pero se va produciendo poco a poco cierto progreso. 

El 09.03.2007 la corporación nVidia publicó la versión 1.99 del controlador nv para Xorg. Éste ofrece ahora soporte para las tarjetas G80. Brevemente tras el lanzamiento de esa versión, darktama, Thunderbird y Marcheu analizaron el código en busca de nuevos tipos de información. Es necesario realizar un trabajo de desenmarañado del código para añadirlo a nuestro código actual. 

En el frente de [[MmioTrace|MmioTrace]], se han reunido todas las ramas en la rama estable, por lo que todas tus funcionalidades favoritas ya están disponibles en ella. 

Thunderbird y Skinkie están intentando realizar la ingeniería inversa de la salida de TV (TV-Out) en tarjetas NVidia. Se pudo identificar algunos registros que regulan funciones como el ajuste del parpadeo, el sobremuestreo, o el posicionamiento de imagen, pero todavía queda mucho trabajo por hacer. Thunderbird conoce bastante los chips de NVidia, ya que mantiene y desarrolla nvclock. Él está interpretando las tablas y datos de la BIOS, mientras que Skinkie ha decidido investigar los datos que obtenidos a través de [[MmioTrace|MmioTrace]]. 

pmdata y jwstolk se encontraron con serios problemas al usar glxgears en una NV15. En cuanto se enviaba la primera instrucción FIFO al hardware, el sistema se bloqueaba. Intentaron reducir el número de instrucciones, insertar retardos (NOPs) en la cola, pero únicamente cuando la FIFO recibía instrucciones "NOP" funcionaba el sistema (um, hacía que no se bloquease, pero, obviamente, una No OPeración no producía mucho trabajo de visualización :) ). Todavía se está trabajando en ello. 

Estamos trabajando actualmente en la creación de una herramienta de línea de comandos para ayudar a los inexperimentados a hacer de forma automática los volcados de nouveau. La primera versión experimental pronto será publicada. Se puede encontrar la primera versión aquí: [[http://www.ping.de/sites/koala/script/createdump.sh|http://www.ping.de/sites/koala/script/createdump.sh]] Si la pruebas, informa a KoalaBR cómo te ha ido. 


### Ayuda necesaria

Remitimos a nuestra página de estado [[http://users.tkk.fi/~jpakkane/ren/|http://users.tkk.fi/~jpakkane/ren/]] para los volcados de renouveau. Solamente necesitamos volcados de las tarjetas que se muestran en rojo en la lista. Nos interesarían, sin embargo, volcados de tarjetas SLI (por favor, indica en el asunto del correo que se trata de un volcado SLI). También sería de agadecer la ayuda de algún propietario de una G80 que esté dispuesto a probar parches. 

Los volcados de [[MmioTrace|MmioTrace]] son todavía muy bienvenidos. 

Y el diluvio de gente que mencionamos al principio hizo patente que necesitamos gente en el canal que contesten a las preguntas de los recién llegados. La mayoría de las respuestas están disponibles en el Wiki / FAQ. 

Las personas a las que les gustaría participar en el Google Summer of Code, deberían contactar con marcheu. Disponemos de algunas plazas disponibles bajo el amparo de Xorg. Sería estupendo si incluso se presentase un proyecto que se quisiese llevar a cabo. Se pueden encontrar algunas ideas en la página [[ToDo|ToDo]], pero presenta con libertad otras ideas que puedas tener. Si no tienes ninguna, nosotros seguro que podríamos ofrecer una o dos para empezar. A medida que pase el tiempo añadiremos una página dedicada al SoC en el wiki. 

Y finalmente: Maximi89 estaría muy interesado en traducir los TiNDCs y el Wiki al español. Como siempre, le gustaría que otras personas le echasen una mano en la tarea. En general, nos gustaría ofrecer páginas traducidas, por lo que no seas tímido y ¡ponte a ello!. Si quieres traducir el TiNDC también, házmelo saber. Estaría dispuesto a mandar el borrador un día antes de ponerlo en línea, para que las traducciones no estuviesen listas más tarde. 

Y viendo que también vamos ganando lentamente una versión francesa, me pregunto... ¿dónde están mis colegas alemanes? 

Bien, y esto es todo en esta ocasión. Espero que hayas disfrutado con esta lectura. 
