<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="/wiki/translate.js"></script>

## About this page

<small>Hello there. You would like to help Nouveau and maybe even become a developer. You have hung out on #nouveau IRC channel at Freenode, but you might not understand much about what the people are saying there. Maybe you do not have much of a background with graphics systems, but you would like to learn things before trying to jump in. 

This page is for you. Here are gathered links to other documents in a comprehensible order. You can start reading the first ones to get a bigger picture of what the graphics world looks like. Going further down the list, the documents get more demanding, technical and specific. We recommend maintaining the rough order while reading through these. 

If you know good documents that should be linked here, go ahead and add them.</small> 
## Introduction to the context

[[The State of Linux Graphics|http://sites.google.com/site/jonsmirl/graphics]] - by Jon Smirl, 2005 

* <small>Read it if you are interested on how [[X Rendering Extension (Render or XRender)|http://en.wikipedia.org/wiki/XRender]] and [[EXA Acceleration Architecture|http://en.wikipedia.org/wiki/EXA]] came into life.</small> 

## Mesa

Mesa is an open-source implementation of the OpenGL specification - a system for rendering interactive 3D graphics. A variety of device drivers allows Mesa to be used in many different environments ranging from software emulation to complete hardware acceleration for modern GPUs. Mesa ties into several other open-source projects: the Direct Rendering Infrastructure (DRI) and X.org to provide OpenGL support. 
## DRI - Direct Rendering Infrastructure

The Direct Rendering Infrastructure, also known as the DRI, is a framework for allowing direct access to graphics hardware under the X Window System in a safe and efficient manner. It is provided by the X server, several client libraries, and the kernel (DRM, Direct Rendering Manager). 

1. [[Direct Rendering Infrastructure wiki|http://dri.freedesktop.org/wiki/]] 
1. [[Introduction to the Direct Rendering Infrastructure|http://dri.sourceforge.net/doc/DRIintro.html]] by Brian Paul, 2000 
    * <small>General introduction to what DRI is all about. If you are interested in 3d, read section 5, _DRI Components_.</small> 
1. [[DRI Explanation|http://people.freedesktop.org/~ajax/dri-explanation.txt]] - the previous article covers the same and has better description however.

## A peek inside

1. [[How Videocards Work|http://wiki.x.org/wiki/Development/Documentation/HowVideoCardsWork]], article in X.org wiki. 
* The parts that make a videocard, mostly from an Ati/Radeon point of view. 
1. [[GraphicStackOverview|GraphicStackOverview]] by Sylvain Bertrand 
     * Links to design documents. If you don't know TMDS, LVDS and so on, have a look here. 
1. [[IrcChatLogs|IrcChatLogs]], primarily the links to The Irregular Nouveau Development Companions 
     * If you want to know some Nouveau history, these are the stories. Skip if you wish. 
1. [[Introduction to Gallium3D|http://jrfonseca.blogspot.com/2008/04/gallium3d-introduction.html]] by José Fonseca. 
     * <small>This is the future and present of accelerated Open Source 3D graphics, the driver architecture used by e.g. Nouveau.</small> 

## Compilers

1. [Collection of articles related to compilers](http://cwabbottdev.blogspot.de/2013/06/compiler-theory-links.html)

## Nvidia-specific introduction

1. Gentle [[Introductory Course|https://github.com/pathscale/pscnv/wiki/Introductorycourse]] on Nvidia's Hardware with explanations of the basics as well 
1. [[NouveauTerms|NouveauTerms]] - A list of terms specific to Nouveau 
1. How does the graphics pipeline map on modern NVIDIA hardware?
    * An NVIDIA [[blogpost|https://developer.nvidia.com/content/life-triangle-nvidias-logical-pipeline]], which is more compact, but has less information, than the following video.
    * An NVIDIA [[talk|http://on-demand.gputechconf.com/gtc/2016/video/S6138.html]] at GTC 2016, and its [[slides|http://on-demand.gputechconf.com/gtc/2016/presentation/s6138-christoph-kubisch-pierre-boudier-gpu-driven-rendering.pdf]]
1. [[riva128.txt|http://rivatv.sourceforge.net/stuff/riva128.txt]] 
     * Old document about a very old graphics card, but gives an idea how things used to work. 
1. [[ContextSwitching|ContextSwitching]] What is context switching, why is it important, how does it work... 
1. [[New DRI Memory Manager|xdevconf2006.pdf]] (PDF) by Keith Whitwell and Thomas Hellstrom, 2006 
     * Introduction slides about Translation Table Maps, TTM. 
1. [[ArthurHuillet|ArthurHuillet]] (Information about XVideo and other topics) 
1. [[XVideo slides|http://people.freedesktop.org/~mhopf/fosdem_2007_xvideo_opengl.pdf]] (if you want to work on Xvideo) 
1. [[NV40 Architecture|http://www.digit-life.com/articles2/gffx/nv40-part1-a.html]], article at digit-life.com. 
     * What is inside and around a fairly modern GPU, but does not tell anything about programming it. 
1. [[GL_NV_fence extension|http://www.khronos.org/registry/gles/extensions/NV/fence.txt]] - Explains what is _fencing_. 

## Obsolete documents (from the viewpoint of Nouveau since we use Gallium for 3D)

1. [[DRI data flow diagram|http://dri.sourceforge.net/doc/dri_data_flow.html]] (Quick view of DRI data flows) 
1. [[DRI control flow diagram|http://dri.sourceforge.net/doc/dri_control_flow.html]] (Quick view of DRI control flows) 

## Getting to work

Pick your area of interest or read our [[ToDo|ToDo]], come to our IRC channel and subscribe to our mailing list, if you have not already done so and get involved. 
