<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="/wiki/translate.js"></script>

### Reviews about Nvidia chips
[[!table header="no" class="mointable" data="""
 **Nvidia chip**  |  **Informations** 
 NV4 (TNT)  |  [[Ars technica|http://arstechnica.com/reviews/1q99/gainward-tnt-1.html]] [[Beyond3D|http://www.beyond3d.com/resources/chip/34]]
 NV5 (TNT2)  |  [[Nvidia|http://www.nvidia.com/page/tnt2.html]] Anandtech [[(1)|http://www.anandtech.com/showdoc.aspx?i=940]] [[(2)|http://www.anandtech.com/showdoc.aspx?i=1058]] [[Beyond3D|http://www.beyond3d.com/resources/chip/35]]
 NV6 (Vanta) [[extensions|Nv04Extensions]]  |  [[Nvidia|http://www.nvidia.com/page/vanta.html]]
 NV10 (Geforce)  |  [[Nvidia|http://www.nvidia.com/page/geforce256.html]] Ars technica [[(1)|http://arstechnica.com/reviews/1q00/geforces/gfru-1.html]] [[(2)|http://arstechnica.com/reviews/1q00/asusv6600/v6600-1.html]] [[Anandtech|http://www.anandtech.com/showdoc.aspx?i=1107]] [[Beyond3D|http://www.beyond3d.com/resources/chip/2]]
 NV11 (Geforce 2 MX,Go)  |  Nvidia [[(1)|http://www.nvidia.com/page/geforce2.html]] [[(2)|http://www.nvidia.com/page/geforce2mx.html]] [[(3)|http://www.nvidia.com/page/geforce2go.html]] [[Anandtech|http://www.anandtech.com/showdoc.aspx?i=1266]] [[Sharky extreme|http://www.sharkyextreme.com/hardware/articles/comdex_2000/nvidia_geforce2_go/index.shtml]] [[Beyond3D|http://www.beyond3d.com/resources/chip/37]]
 NV15 (Geforce 2 Pro,Ti,Ultra,Gts) [[extensions|Nv15Extensions]]  |  Nvidia [[(1)|http://www.nvidia.com/page/geforce2.html]] [[(2)|http://www.nvidia.com/page/geforce2pro.html]] Tom's hardware [[(1)|http://www.tomshardware.com/2000/04/27/tom/index.html]] [[(2)|http://www.tomshardware.com/2000/08/14/nvidia_strikes_back_/index.html]] [[Ars technica|http://arstechnica.com/reviews/2q00/geforce2gts/gts-1.html]] Anandtech[[(1)|http://www.anandtech.com/showdoc.aspx?i=1231]] [[(2)|http://www.anandtech.com/showdoc.aspx?i=1377]] [[(3)|http://www.anandtech.com/showdoc.aspx?i=1298]] [[Sharky extreme|http://www.sharkyextreme.com/hardware/guides/nvidia_geforce2_ultra/index.shtml]] [[Tech report|http://techreport.com/reviews/2000q2/geforce2/]] [[Beyond3D|http://www.beyond3d.com/resources/chip/38]]
 NV17,NV18 (Geforce 4 MX,Go)  |  [[Nvidia|http://www.nvidia.com/page/geforce4mx.html]] [[Viperlair|http://www.viperlair.com/articles/archive/launch/nvidia/gf4mx.shtml]] [[Techspot|http://www.techspot.com/reviews/hardware/evga_gf4mx440/]] [[Guru3D|http://www.guru3d.com/review/creative/geforce4mx420/]] [[Beyond3D|http://www.beyond3d.com/resources/chip/4]]
 NV20 (Geforce 3)  |  [[Nvidia|http://www.nvidia.com/page/geforce3.html]] [[Sharky extreme|http://www.sharkyextreme.com/hardware/previews/nvidia_geforce3_hands-on/index.shtml]] [[Beyond3D|http://www.beyond3d.com/resources/chip/6]]
 NV25,NV28 (Geforce 4 Ti)  |  [[Nvidia|http://www.nvidia.com/page/geforce4ti.html]] [[Beyond3D|http://www.beyond3d.com/resources/chip/8]]
 NV3x (Geforce FX 5x00)  |  Nvidia [[(1)|http://www.nvidia.com/page/fx_5200.html]] [[(2)|http://www.nvidia.com/page/fx_5700.html]] Beyond3D [[(1)|http://www.beyond3d.com/resources/chip/9]] [[(2)|http://www.beyond3d.com/resources/chip/10]] [[(3)|http://www.beyond3d.com/resources/chip/27]]
 NV4x (Geforce 6x00)  |  [[Nvidia|http://www.nvidia.com/page/geforce6.html]] Beyond3D [[(1)|http://www.beyond3d.com/resources/chip/62]] [[(2)|http://www.beyond3d.com/resources/chip/86]] [[(3)|http://www.beyond3d.com/resources/chip/71]] [[(4)|http://www.beyond3d.com/resources/chip/72]]
 G7x (Geforce 7x00)  |  [[Nvidia|http://www.nvidia.com/page/geforce7.html]] Beyond3D [[(1)|http://www.beyond3d.com/resources/chip/106]] [[(2)|http://www.beyond3d.com/resources/chip/108]]
 G8x (Geforce 8x00)  |  [[Nvidia|http://www.nvidia.com/page/geforce8.html]] Beyond3D [[(1)|http://www.beyond3d.com/resources/chip/117]] [[(2)|http://www.beyond3d.com/content/reviews/1]] [[(3)|http://www.beyond3d.com/resources/chip/121]] [[(4)|http://www.beyond3d.com/content/reviews/11]]
"""]]
