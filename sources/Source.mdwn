<meta name="google-translate-customization" content="38b387022ed0f4d4-a4eb7ef5c10c8ae0-g2870fab75904ce51-18"></meta>
<div id="google_translate_element"></div>
<script type="text/javascript" src="/wiki/translate.js"></script>

The current code is in git trees on [[freedesktop.org|http://cgit.freedesktop.org/]].

* [[DDX git|http://cgit.freedesktop.org/nouveau/xf86-video-nouveau/]] (2D driver) holds the accelerated Nouveau Xorg driver.
   * This was forked from nVidia’s [[nv driver|http://xorg.freedesktop.org/wiki/nv]] a long time ago.
   * Git address: `git://anongit.freedesktop.org/git/nouveau/xf86-video-nouveau`
* [[DRM git|http://cgit.freedesktop.org/mesa/drm/]] (DRM userspace libraries) holds libdrm and libdrm_nouveau.
   * These provide a userspace interface for the kernel’s [[direct rendering manager|http://dri.freedesktop.org/wiki/DRM]].
   * Git address: `git://anongit.freedesktop.org/git/mesa/drm`
* [[Nouveau kernel tree|http://cgit.freedesktop.org/nouveau/linux-2.6/]] (DRM modules) holds the latest `nouveau` kernel driver.
   * Git address: `git://anongit.freedesktop.org/git/nouveau/linux-2.6`
   * Out-of-tree module git: `git://people.freedesktop.org/~darktama/nouveau`
   * [[Build instructions|InstallNouveau]].
   * Shortcut to the [[code|http://cgit.freedesktop.org/nouveau/linux-2.6/tree/drivers/gpu/drm/nouveau]] and [[log|http://cgit.freedesktop.org/nouveau/linux-2.6/log/drivers/gpu/drm/nouveau]] in git.
* [[Mesa master|http://cgit.freedesktop.org/mesa/mesa/]] is the place for 3D driver development.
   * Git address: `git://anongit.freedesktop.org/git/mesa/mesa`
   * Shortcuts to Mesa master branch:
      * the classic driver (< NV30): nouveau_vieux [[git log|http://cgit.freedesktop.org/mesa/mesa/log/src/mesa/drivers/dri/nouveau]], [[code|http://cgit.freedesktop.org/mesa/mesa/tree/src/mesa/drivers/dri/nouveau]]
      * the gallium3d driver (>= NV30): nv30, nv50 and nvc0 [[git log|http://cgit.freedesktop.org/mesa/mesa/log/src/gallium/drivers/nouveau]], [[code|http://cgit.freedesktop.org/mesa/mesa/tree/src/gallium/drivers/nouveau]]
