# CI

This wiki is automatically regenerated through the gitlab CI pipeline running after every push to the master branch and on every Merge request. The Preview can be found on the Merge Request below the CI Pipeline, which will pointer towards the artifact browser listing all the generated files.

# Testing

Generation of the wiki can be tested locally by executing `ikiwiki --setup nouveau.yml`. Output files are stored in `./public/`.

Some optional perl modules need to be installed in addition to `ikiwiki`.

* Text::CSV
* Text::MultiMarkdown
